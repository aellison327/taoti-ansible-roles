## Instructions
### First Run (Only do these once)
1. Install https://www.virtualbox.org/wiki/Downloads
2. Install http://www.vagrantup.com/
3. ``$ brew install ansible``
4. ``$ vagrant box add trusty64 https://vagrantcloud.com/ubuntu/trusty64/version/1/provider/virtualbox.box``
### For each site
5. ``$ git clone git@bitbucket.org:taoticreative/taoti-ansible-roles.git ~/some/project/path``
6. ``$ cd ~/some/project/path``
7. Copy/Rename Vagrantfile.example to Vagrantfile
8. Edit the Vagrantfile ip and any additional options.
9. ``$ vagrant up`` Wait for the box to build (this takes 5 minutes and is quicker in subsequent runs)
10. ``$ vagrant ssh``
11. ``$ cd /var/www``
12. ``$ rm -rf ./html``
13. Clone your new project into the html folder and install your site as normal.

## More Info
### Commonly Used Variables
Most role variables can be left alone, here are some more common ones you may want to change however (line breaks may
be off). Best practices would include creating a vars/role.yml file for each of these different roles but lumping them
into a single file.yml will also work. Make sure to add a ``var_files:`` section according to the playbook.yml.example.

``apache_vhosts:
    - {servername: "local.dev", documentroot: "/var/www/html"}
    - {servername: "secondsite.dev", documentroot: "/var/www/secondsite"}``

``mysql_root_password: root``
### Shared folders

You shared folders are defined in the Vagrantfile. The example shares 2 different folders which should be good for
basic projects, you can add/remove them as you would like to work however by editing the Vagrantfile and running
``$ vagrant reload``
An example of the www share is:

``config.vm.synced_folder "www", "/var/www",``
``id: "vagrant-root",

       create: true,

       owner: 'vagrant',

       group: 'www-data',

       mount_options: ["dmode=775,fmode=775"]``

#### Note about data
A default databag is defined in $VMRoot/data which is mounted as /vagrant/data on the VirtualMachine.